/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.lang.reflect.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import model.Cart;
import model.Category;
import model.Customer;
import model.CustomerDetail;
import model.Item;
import model.Product;
import javax.mail.*;
import javax.mail.internet.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import model.Order;
import model.OrderDetail;
import model.ProductView;

/**
 *
 * @author trant
 */
public class DAO extends DBContext {

    public static void sendEmail(String to, String subject, String body) {
        try {

            InitialContext initialContext = new InitialContext();
            Session session = (Session) initialContext.lookup("java:comp/env/mail/Session");

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(session.getProperty("mail.smtp.username")));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(body);

            Transport.send(message);
        } catch (NamingException | MessagingException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String to = "dattthe163068@fpt.edu.vn";
        String subject = "dâdada";
        String body = "dddddddddddddddd";
        sendEmail(to, subject, body);
    }
//set role

    public void setRole(int role, int id) {
        String sql = "update customerdetail set role=? where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, role);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int getRoleById(int id) {
        String sql = "select role from customerdetail where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("role");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    // check xem co tai khoan trong database ko
    public Customer getAccount(String username, String password) {
        String sql = "SELECT TOP (1000) [id]\n"
                + "      ,[name]\n"
                + "      ,[amount]\n"
                + "      ,[username]\n"
                + "      ,[password]\n"
                + "  FROM [CartDBrerach].[dbo].[Customer]\n"
                + "  where username=? and password=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Customer(rs.getInt("id"), rs.getString("name"), rs.getDouble("amount"), username, password);
            }
        } catch (SQLException e) {

        }
        return null;
    }
//getRole

    public int getRole(int id) {
        String sql = "select role from customerDetail where id=" + id + " ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("role");
            }
        } catch (SQLException e) {

        }
        return 0;

    }

    ///// danh sach san pham
    public List<Product> getALL() {

        List<Product> list = new ArrayList<>();
        String sql = "select*from Products where price<30000000";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setPrice(rs.getDouble("price"));
                p.setQuantity(rs.getInt("quantity"));
                p.setImage(rs.getString("image"));
                p.setReleaseDate(rs.getDate("releasedate"));
                p.setDescribe(rs.getString("describe"));
                Category c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);
                //
                list.add(p);
            }

        } catch (SQLException e) {

        }
        return list;
    }
    // check dup uswername

    public boolean checkDuplicateUserName(String username) {
        String sql = "SELECT TOP (1000) [id]\n"
                + "      ,[name]\n"
                + "      ,[amount]\n"
                + "      ,[username]\n"
                + "      ,[password]\n"
                + "  FROM [CartDBrerach].[dbo].[Customer]"
                + "where username=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {

        }

        return false;
    }

    public boolean checkDuplicateIDofProduct(int id) {
        String sql = "select * from products where id=" + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {

        }

        return false;
    }

    ///// danh sach san pham co id=?
    public Product getProductById(int id) {

        String sql = "select*from Products where id=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setPrice(rs.getDouble("price"));
                p.setQuantity(rs.getInt("quantity"));
                Category c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);
                // list.add(p);
                return p;
            }

        } catch (SQLException e) {

        }
        // ko tim thay
        return null;
    }
    // get productview

    public ProductView getProductViewById(int id) {
        ProductView p = new ProductView();
        String sql = "select*from Products where id=?";
        String sql1 = "select*from Productsdetail where pid=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setPrice(rs.getDouble("price"));
                p.setQuantity(rs.getInt("quantity"));
                p.setImage(rs.getString("image"));
                p.setReleaseDate(rs.getDate("releaseDate"));
                p.setDescribe(rs.getString("describe"));
                Category c = new Category();
                c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);

                //
            }

        } catch (SQLException e) {

        }
        // sql1

        try {
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, id);
            ResultSet rs1 = st1.executeQuery();
            if (rs1.next()) {

                p.setRAM(rs1.getInt("RAM"));
                p.setCPU(rs1.getString("CPU"));
                p.setGPU(rs1.getString("GPU"));
                p.setDisplay(rs1.getString("Display"));
                Product pro = new Product();
                pro = getProductById(id);
                p.setProduct(pro);

                // list.add(p);
                return p;
            }

        } catch (SQLException e) {

        }
        // ko tim thay
        return null;
    }

    public void addOrder(Customer c, Cart cart) {
        LocalDate currentDate = LocalDate.now();
        String date = currentDate.toString();
        try {
            // add vao order
            String sql = "INSERT INTO [dbo].[Order]\n"
                    + "           ([date]\n"
                    + "           ,[cid]\n"
                    + "           ,[totalmoney])\n"
                    + "     VALUES\n"
                    + "           (?,?,?)";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, date);
            st.setInt(2, c.getId());
            st.setDouble(3, cart.getTotalMoney());
            st.executeUpdate();
            // lay id cua order vua add
            String sqll = "select top 1 id from [Order] order by id desc ";
            PreparedStatement st1;
            st1 = connection.prepareStatement(sqll);
            ResultSet rs = st1.executeQuery();
            // add vao ?derdetail
            if (rs.next()) {
                int oid = rs.getInt("id");
                for (Item item : cart.getItems()) {
                    String sql2 = "insert into [Orderline] values(?,?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, oid);
                    st2.setInt(2, item.getProduct().getId());
                    st2.setInt(3, item.getQuantity());
                    st2.setDouble(4, item.getPrice());

                    st2.executeUpdate();

                }
            }
            // cap nhat lai so luong san pham
            String sql3 = "update products set quantity=quantity-? where id=?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for (Item item : cart.getItems()) {
                st3.setInt(1, item.getQuantity() / 2);
                st3.setInt(2, item.getProduct().getId());
                st3.executeUpdate();
            }
            st3.executeUpdate();

            //cap nhat amount cua customer
            String sql4 = "update Customer set amount=amount-? where id=?";
            PreparedStatement st4 = connection.prepareStatement(sql4);
            st4.setDouble(1, cart.getTotalMoney());
            st4.setInt(2, c.getId());
            st4.executeUpdate();

        } catch (SQLException e) {

        }
    }

    public int getlatest() {
        String sql = "select top 1 id from customer order by id desc ";
        try {

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return rs.getInt("id");
            }

        } catch (SQLException e) {
        }
        return 0;
    }

    int a = 3131;

    public void addUser(String name, String username, String password, String email, String phone, String country) {
      
        try {
            String sql = "INSERT INTO [dbo].[Customer]\n"
                    + "           ([id]\n"
                    + "           ,[name]\n"
                    + "           ,[amount]\n"
                    + "           ,[username]\n"
                    + "           ,[password])\n"
                    + "     VALUES\n"
                    + "          (?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
           
            
            int b = getlatest() + 1;
            st.setInt(1, b);
            st.setString(2, name);
            st.setDouble(3, 1000000000);
            st.setString(4, username);
            st.setString(5, password);

            st.executeUpdate();
            ///////////////
            String sql1 = "INSERT INTO [dbo].[CustomerDetail] values(?,?,?,?,?)";

            PreparedStatement st1 = connection.prepareStatement(sql1);

            st1.setInt(1, b);
            st1.setString(2, email);
            st1.setString(3, phone);
            st1.setString(4, country);
            st1.setInt(5, 1);
            st1.executeUpdate();

        } catch (SQLException e) {
            
        }

    }

    public void addProduct(int id, String name, int quantity, double price, Date releaseDate, String describe,
            String image, int cid, int RAM, String CPU, String GPU, String Display) {
        java.util.Date date = releaseDate;
        java.sql.Date sqldate = new java.sql.Date(date.getTime());
        //
        String sql1 = "insert into ProductsDetail values(?,?,?,?,?)";
///
        String sql = "INSERT INTO [dbo].[Products]\n"
                + "           ([id]\n"
                + "           ,[name]\n"
                + "           ,[price]\n"
                + "           ,[quantity]\n"
                + "           ,[image]\n"
                + "           ,[releaseDate]\n"
                + "           ,[describe]\n"
                + "           ,[cid])\n"
                + "     VALUES\n"
                + "          (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.setString(2, name);
            st.setDouble(3, price);
            st.setInt(4, quantity);
            st.setString(5, image);
            st.setDate(6, sqldate);
            st.setString(7, describe);

            st.setInt(8, cid);

            //
            st.executeUpdate();

        } catch (SQLException e) {
        }
        try {
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, RAM);
            st1.setString(2, CPU);
            st1.setString(3, GPU);
            st1.setString(4, Display);
            st1.setInt(5, id);
            st1.executeUpdate();
        } catch (SQLException e) {
        }

    }

    public void deleteProduct(int id) {
        String sql = "delete from Products where id=?";
        String sql1 = "delete from Productsdetail where pid=?";
        try {

            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, id);

            st1.executeUpdate();
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {

        }

    }

    public void test() {

        java.util.Date currentDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(currentDate.getTime());

        String sql = "INSERT INTO [dbo].[Products]\n"
                + "                        values (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st1 = connection.prepareStatement(sql);
            st1.setInt(1, 8);
            st1.setString(2, "dsdddd");
            st1.setDouble(3, 323);
            st1.setInt(4, 3);
            st1.setString(5, "dsd");
            st1.setDate(6, sqlDate);
            st1.setString(7, "dssds");

            st1.setInt(8, 1);

            st1.executeUpdate();

        } catch (SQLException e) {
        }

    }

    public List<Product> search(String key, java.sql.Date from, java.sql.Date to, Double price1, Double price2,
            int cid) {

        List<Product> list = new ArrayList<>();

        String sql = "select * from products p left join dbo.ProductsDetail pd on  p.id=pd.pid\n"
                + "where 1=1 ";

        if (key != null && !key.equals("")) {
            sql += "and name like '%" + key + "%'  or describe like '%" + key + "%' or RAM like '%" + key
                    + "%' or CPU like '%" + key + "%'or GPU like '%" + key + "%' or Display like '%" + key + "%'";
        }
        if (from != null) {
            sql += "and releaseDate>='" + from + "'";
        }
        if (to != null) {
            sql += "and releaseDate<='" + to + "'";
        }
        if (price1 != null) {
            sql += "and price>=" + price1;
        }
        if (price2 != null) {
            sql += "and price<=" + price2;
        }
        if (cid != 0) {
            sql += "and cid=" + cid;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setQuantity(rs.getInt("quantity"));
                p.setPrice(rs.getDouble("price"));
                p.setReleaseDate(rs.getDate("releasedate"));
                p.setDescribe(rs.getString("describe"));
                p.setImage(rs.getString("image"));
                Category c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);
                list.add(p);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;

    }
    // search relate product

    public List<Product> getProductsByCid(int cid) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from products where cid=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setQuantity(rs.getInt("quantity"));
                p.setPrice(rs.getDouble("price"));
                p.setReleaseDate(rs.getDate("releasedate"));
                p.setDescribe(rs.getString("describe"));
                p.setImage(rs.getString("image"));
                Category c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);
                list.add(p);

            }

        } catch (SQLException e) {
        }
        return list;
    }

    //// searching theo category
    public List<Product> searchGeneral(String key2) {

        List<Product> list = new ArrayList<>();

        String sql = "select * from products where 1=1";

        if (key2 != null && !key2.equals("")) {
            /// ngai sua cho nay vl:))))

            sql += "and name like '%" + key2 + "%'  or describe like '%" + key2 + "%'  ";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setQuantity(rs.getInt("quantity"));
                p.setPrice(rs.getDouble("price"));
                p.setReleaseDate(rs.getDate("releasedate"));
                p.setDescribe(rs.getString("describe"));
                p.setImage(rs.getString("image"));
                Category c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);
                list.add(p);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;

    }

    public List<Product> RandomProduct(String key) {

        List<Product> list = new ArrayList<>();

        String sql = "select * from products where 1=1";

        if (key != null && !key.equals("")) {
            sql += "and name like '%" + key + "%'  or describe like '%" + key + "%'  ";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setQuantity(rs.getInt("quantity"));
                p.setPrice(rs.getDouble("price"));
                p.setReleaseDate(rs.getDate("releasedate"));
                p.setDescribe(rs.getString("describe"));
                p.setImage(rs.getString("image"));
                Category c = getCategoryByCid(rs.getInt("cid"));
                p.setCategory(c);
                list.add(p);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;

    }

    public Category getCategoryByCid(int Cid) {
        String sql = "select*from Categories where id =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, Cid);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Category c = new Category();
                c.setId(rs.getInt("id"));
                c.setName(rs.getString("name"));
                c.setDescribe(rs.getString("describe"));
                return c;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public void modifyAccount(int id, String name, String password, String email, String phone, String country) {
        try {
            String sql = "UPDATE [dbo].[Customer]\n"
                    + "   SET \n"
                    + "     [name] = ? \n"
                    + "      \n"
                    + "      ,[password] = ?\n"
                    + " WHERE  id=" + id;
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, name);
            st.setString(2, password);

            st.executeUpdate();
            st.close();
            ///////////////
            String sql1 = "UPDATE [dbo].[CustomerDetail]\n"
                    + "   SET \n"
                    + "      [EMAIL ADDRESS] = ?\n"
                    + "      ,[PHONE NUMBER] = ?\n"
                    + "      ,[COUNTRY] = ?\n"
                    + " WHERE id=" + id;
            PreparedStatement st1 = connection.prepareStatement(sql1);

            st1.setString(1, email);
            st1.setString(2, phone);
            st1.setString(3, country);
            st1.executeUpdate();
            st1.close();

        } catch (SQLException e) {
        }

    }
    // modify product

    public void modifyProduct(int id, String name, Double price, int quantity, String image, java.sql.Date date,
            String describe, int cid,
            int RAM, String CPU, String GPU, String Display) {
        try {
            String sql1 = "UPDATE [dbo].[ProductsDetail]\n"
                    + "   SET [RAM] = ?\n"
                    + "      ,[CPU] =?\n"
                    + "      ,[GPU] =?\n"
                    + "      ,[DISPLAY] =?\n"
                    + "      \n"
                    + " WHERE pid=?";
            PreparedStatement st1 = connection.prepareStatement(sql1);

            st1.setInt(1, RAM);
            st1.setString(2, CPU);
            st1.setString(3, GPU);
            st1.setString(4, Display);
            st1.setInt(5, id);

            st1.executeUpdate();
            ///
            String sql = "UPDATE [dbo].[Products]\n"
                    + "SET [name] = ?\n"
                    + "   ,[price] = ?\n"
                    + "   ,[quantity] = ?\n"
                    + "   ,[image] = ?\n"
                    + "   ,[releasedate] = ?\n"
                    + "   ,[describe] = ?\n"
                    + "   ,[cid] = ?\n"
                    + "WHERE id = " + id;
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, name);
            st.setDouble(2, price);
            st.setInt(3, quantity);
            st.setString(4, image);
            st.setDate(5, date);
            st.setString(6, describe);
            st.setInt(7, cid);

            st.executeUpdate();

            ///////////////
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Customer getCustomerByUserName(String username) {

        try {
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[amount]\n"
                    + "      ,[username]\n"
                    + "      ,[password]\n"
                    + "  FROM [dbo].[Customer]"
                    + "where username=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Customer c = new Customer();
                c.setId(rs.getInt("id"));
                c.setName(rs.getString("name"));

                c.setAmount(rs.getDouble("amount"));
                c.setCustomername(rs.getString("username"));
                c.setPassword(rs.getString("password"));
                return c;
            }

            ///////////////
        } catch (SQLException e) {
        }
        return null;
    }

    //get all customer
    public List<Customer> getCustomer() {
        List<Customer> list = new ArrayList<>();
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[amount]\n"
                    + "      ,[username]\n"
                    + "      ,[password]\n"
                    + "  FROM [dbo].[Customer]";

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Customer c = new Customer();
                c.setId(rs.getInt("id"));
                c.setName(rs.getString("name"));

                c.setAmount(rs.getDouble("amount"));
                c.setCustomername(rs.getString("username"));
                c.setPassword(rs.getString("password"));
                list.add(c);
            }

            ///////////////
        } catch (SQLException e) {
        }
        return list;
    }

    public CustomerDetail getCustomerDetailById(int id) {

        try {
            String sql = "select * from customerdetail where id=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                CustomerDetail c = new CustomerDetail();
                c.setId(rs.getInt("id"));
                c.setEmailAddress(rs.getString("email address"));
                c.setPhoneNumber(rs.getString("phone number"));
                c.setCountry(rs.getString("country"));
                c.setRole(1);
                return c;
            }

            ///////////////
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Order> getOrder() {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT *\n"
                + "  FROM [CartDBrerach].[dbo].[Order]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = new Order(rs.getInt(1), rs.getDate(2), rs.getInt(3), rs.getDouble(4));
                list.add(o);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<OrderDetail> getOrderDetail() {
        List<OrderDetail> list = new ArrayList<>();

        String sql = "select * from orderline ";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                OrderDetail o = new OrderDetail(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
                list.add(o);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //
    public List<OrderDetail> getOrderDetailByOid(int oid) {
        List<OrderDetail> list = new ArrayList<>();

        String sql = "select * from orderline where oid=? ";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, oid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                OrderDetail o = new OrderDetail(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
                list.add(o);
            }
        } catch (Exception e) {
        }
        return list;
    }
}
