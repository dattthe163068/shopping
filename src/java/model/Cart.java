/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DAO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trant
 */
public final class Cart {

    private List<Item> items;

    public Cart() {
        items = new ArrayList<>();
    }

    public Cart(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public int getQuantityById(int id) {
        Item i = getItemById(id);
        return i.getQuantity();
    }

    private Item getItemById(int id) {
        for (Item i : items) {
            if (i.getProduct().getId() == id) {
                return i;
            }

        }
        return null;
    }

public void addItem(Item item) {
    Item it;
    
        if (getItemById(item.getProduct().getId()) != null) {
            it = getItemById(item.getProduct().getId());
            
                 it.setQuantity(it.getQuantity() + item.getQuantity());
            
           
        } else {
            items.add(item);
        }
    
}


    public void deleteItemByItem(Item item) {
        Item it;
        it = getItemById(item.getProduct().getId());
        items.remove(it);
    }

    public void removeItemById(int id) {

        Item item = getItemById(id);
        if (item != null) {
            items.remove(item);

        }

    }

    public double getTotalMoney() {
        double total = 0;
        for (Item item : items) {
            total += item.getQuantity() * (item.getPrice());
        }
        return total;
    }

    public Product getProductById(int id, List<Product> list) {
        for (Product product : list) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    //
    public  Cart(String txt, List<Product> list) {
        items = new ArrayList<>();
        try {
            if (txt != null && txt.length() != 0) {
                String[] str = txt.split("/");
                for (String s : str) {
                    String[] n = s.split(":");
                    int id = Integer.parseInt(n[0]);
                    int quantity=Integer.parseInt(n[1]);
                    DAO d= new DAO();
   
                    Product p =d.getProductById(id);
                    Item t=new Item(p,quantity,p.getPrice());
                    addItem(t);
                }
            }
        } catch (NumberFormatException e) {
        }
    }

}
