/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import mail.Mail;
import model.Cart;
import model.Customer;
import model.Item;
import model.Product;

/**
 *
 * @author trant
 */
@WebServlet(name = "checkoutServlet", urlPatterns = {"/checkout"})
public class checkoutServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet checkoutServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet checkoutServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

        DAO d = new DAO();
        List<Product> list;
        list = d.getALL();
        Cookie[] arr = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie cookie : arr) {
                if (cookie.getName().equals("cart")) {
                    txt += cookie.getValue();
                }
            }
        }
        Cart cart = new Cart(txt, list);
        List<Item> items = cart.getItems();
        HttpSession session = request.getSession();

        Customer a = (Customer) session.getAttribute("account");
        //send invoice
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.ROOT);

       String content = "<!DOCTYPE html>\n"
        + "<html lang=\"en\">\n"
        + "<head>\n"
        + "    <meta charset=\"UTF-8\">\n"
        + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
        + "    <title>Invoice</title>\n"
        + "    <style>\n"
        + "        body {\n"
        + "            font-family: Arial, sans-serif;\n"
        + "        }\n"
        + "        h1 {\n"
        + "            text-align: center;\n"
        + "        }\n"
        + "        table {\n"
        + "            width: 100%;\n"
        + "            border-collapse: collapse;\n"
        + "            margin-top: 20px;\n"
        + "        }\n"
        + "        th, td {\n"
        + "            padding: 10px;\n"
        + "            text-align: left;\n"
        + "        }\n"
        + "        th {\n"
        + "            background-color: #f2f2f2;\n"
        + "        }\n"
        + "        img {\n"
        + "            max-width: 100px;\n"
        + "            max-height: 100px;\n"
        + "        }\n"
        + "        h3 {\n"
        + "            text-align: right;\n"
        + "        }\n"
        + "    </style>\n"
        + "</head>\n"
        + "<body>\n"
        + "    <h1>Online Invoice Service</h1>\n"
        + "    <table>\n"
        + "        <tr>\n"
        + "            <th>Image</th>\n"
        + "            <th>Product</th>\n"
        + "            <th>Quantity</th>\n"
        + "            <th>Sub Total</th>\n"
        + "        </tr>";

for (Item item : items) {
    content += "<tr>\n"
            + "    <td><img src=\"" + item.getProduct().getImage() + "\" alt=\"Product Image\"></td>\n"
            + "    <td>" + item.getProduct().getName() + "</td>\n"
            + "    <td>" + item.getQuantity() + "</td>\n"
            + "    <td>" + currencyFormat.format(item.getPrice() * item.getQuantity()) + "</td>\n"
            + "</tr>\n";
}

content += "</table>\n"
        + "<h3>Total Money: " + currencyFormat.format(cart.getTotalMoney()) + "</h3>\n"
        + "</body>\n"
        + "</html>";


        try {
            Mail m = new Mail();
//            Mail.sendInvoice(d.getCustomerDetailById(a.getId()).getEmailAddress(), "Online Invoice", content);
        } catch (Exception e) {

        }
        if (session.getAttribute("name") == null) {
            response.sendRedirect("login.jsp");
        } else {
            d.addOrder(a, cart);
            Cookie c = new Cookie("cart", "");
            c.setMaxAge(0);
            response.addCookie(c);
            session.removeAttribute("size");
            request.getRequestDispatcher("list").forward(request, response);

        }

    }

    ///debug
    public static void main(String[] args) {
        DAO d = new DAO();
        List<Product> list;
        list = d.getALL();
        String txt = "1:2/2:3";
        Cart cart = new Cart(txt, list);
        List<Item> list1 = cart.getItems();

        Customer a = d.getAccount("dat", "123456");

        d.addOrder(a, cart);
        for (Item item : list1) {
            System.out.println(item.getQuantity());

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
