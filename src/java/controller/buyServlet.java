/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.Item;
import model.Product;

/**
 *
 * @author trant
 */
@WebServlet(name = "buyServlet", urlPatterns = {"/buy"})
public class buyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet buyServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet buyServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @param id
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
    @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Alert Example</title>");
        out.println("</head>");
        out.println("<body>");

        // JavaScript code to prompt an alert
        out.println("<script>");
        out.println("alert('This is an alert message from the Servlet!');");
        out.println("</script>");

       
        out.println("</body>");
        out.println("</html>");
        
    }
}


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String num_raw = request.getParameter("num");
        int num = Integer.parseInt(num_raw);

        String id = request.getParameter("id");
        int ID = Integer.parseInt(id);
        if (num == 0 || num_raw.equals("0")) {
           
            response.sendRedirect("productview?id=" + ID + "&msg="+"fail to add");
        }


        DAO d = new DAO();
        List<Product> list;
        list = d.getALL();
        Cookie[] arr = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie cookie : arr) {
                if (cookie.getName().equals("cart")) {
                    txt += cookie.getValue();
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                    // break;
                }
            }
        }
       
        if (txt.isEmpty()) {
            txt = id + ":" + num;

        } else {
            txt = txt + "/" + ID + ":" + num;
        }
        Cookie c = new Cookie("cart", txt);
        c.setMaxAge(2 * 24 * 60 * 60);
        response.addCookie(c);
        //vo van 
        Cart cart = new Cart(txt, list);
        PrintWriter out = response.getWriter();
        out.print(txt);
        List<Item> listItem = cart.getItems();
        HttpSession ss = request.getSession();
        if (listItem != null) {
            ss.setAttribute("size", listItem.size());
        } else {
            ss.setAttribute("size", 0);
        }
        response.sendRedirect("productview?id=" + ID + "");

//        request.getRequestDispatcher("Shop").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void main(String[] args) {

    }
}
