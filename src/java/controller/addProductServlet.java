/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author trant
 */
@WebServlet(name = "addProductServlet", urlPatterns = {"/addProduct"})
public class addProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet addProductServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet addProductServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

        String id_raw = request.getParameter("id");

        String name = request.getParameter("name");
        String price_raw = request.getParameter("price");
        String quantity_raw = request.getParameter("quantity");
        String image = request.getParameter("image");
        String describe = request.getParameter("describe");
        String date_raw = request.getParameter("date");

        String cid_raw = request.getParameter("cid");
        /////
        String RAM_raw = request.getParameter("ram");
        String CPU = request.getParameter("cpu");
        String GPU = request.getParameter("gpu");
        String Display = request.getParameter("display");

        try {
            int RAM = Integer.parseInt(RAM_raw);
            int id = Integer.parseInt(id_raw);
            DAO d = new DAO();

            //
            int cid = Integer.parseInt(cid_raw);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date;
            try {
                date = dateFormat.parse(date_raw);
            } catch (ParseException e) {
                return; // Handle the parse exception
            }

// Now you can work with the "date" object as a Date
            double price = Double.parseDouble(price_raw);
            int quantity = Integer.parseInt(quantity_raw);
            if (d.checkDuplicateIDofProduct(id)) {
                // Show alert in browser if there's a duplicate ID
                String duplicateAlertScript = "<script>alert('Duplicate ID! Please enter a different ID.'); window.location='dash.jsp';</script>";
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.println(duplicateAlertScript);
                out.flush();
            } else {
                d.addProduct(id, name, quantity, price, date, describe, image, cid, RAM, CPU, GPU, Display);

                response.sendRedirect("dash.jsp");
            }

        } catch (Exception e) {
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
