/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author trant
 */
@WebServlet(name = "signupServlet", urlPatterns = {"/signup"})
public class signupServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet signupServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet signupServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static final String NAME_REGEX = "^[a-zA-Z\\s]*$"; // Only letters and spaces allowed
    public static final String USERNAME_PASSWORD_REGEX = "^[a-zA-Z0-9]{1,40}$"; // Alphanumeric, up to 40 characters
    public static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@(.+)$"; // Valid email pattern
    public static final String PHONE_REGEX = "^[0-9]{10,12}$";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        processRequest(request, response);
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String country = request.getParameter("country");
 
        boolean isNameValid = validateInput(name, NAME_REGEX);
        boolean isUsernameValid = validateInput(username, USERNAME_PASSWORD_REGEX);
        boolean isPasswordValid = validateInput(password, USERNAME_PASSWORD_REGEX);
        boolean isEmailValid = validateInput(email, EMAIL_REGEX);
        boolean isPhoneValid = validateInput(phone, PHONE_REGEX);
        DAO d = new DAO();
        boolean check = d.checkDuplicateUserName(username);
        if (checkNullCountry(country)==true || check || !isNameValid || !isUsernameValid || !isPasswordValid || !isEmailValid || !isPhoneValid) {
            if (check) {
                request.setAttribute("duplicate", "duplicate username, please choose another one");
            }
            if (!isNameValid) {
                request.setAttribute("nameerror", "Invalid name format not contain special characters");
            }
            if (!isUsernameValid || !isPasswordValid) {
                request.setAttribute("usernameerror", "Invalid username or password format must be less than 40 characters");
            }
            if (!isEmailValid) {
                request.setAttribute("emailerror", "Invalid email format");
            }
            if (!isPhoneValid) {
                request.setAttribute("phoneerror", "Invalid phone format");
            }
            if (checkNullCountry(country)==true) {
                request.setAttribute("nullcountry", "Null Country Input not allowed");
            }

            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        } else {
            d.addUser(name, username, password, email, phone, country);
            response.sendRedirect("login.jsp");
        }

    }
   public boolean checkNullCountry(String Country){
       if (Country=="") {
           return true;
       }
       return false;
   }
    public boolean validateInput(String input, String regexPattern) {
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
