/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.DAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.Category;
import model.Product;

/**
 *
 * @author trant
 */
@WebServlet(name = "sortServlet", urlPatterns = {"/sort"})
public class sortServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet listServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet listServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        DAO d = new DAO();
        CategoryDAO c = new CategoryDAO();
        List<Category> list = c.getALL();
        ProductDAO db = new ProductDAO();

        request.setAttribute("data", list);
        //
        
         String sort = request.getParameter("customRadio");
        String cid_raw = request.getParameter("key");
        String key = request.getParameter("key2");
        String from_raw = request.getParameter("fromdate");
        String to_raw = request.getParameter("todate");
        String from_price_raw = request.getParameter("fromprice");
        String to_price_raw = request.getParameter("toprice");
        Double price1, price2;
        Date from, to;
        int cid,sizebag;

        try {
            
              
//            int sort=Integer.parseInt(sort_raw);
            cid = (cid_raw == null) ? 0 : Integer.parseInt(cid_raw);
            price1 = ((from_price_raw == null) || (from_price_raw.equals("")))
                    ? null : Double.parseDouble(from_price_raw);
            price2 = ((to_price_raw == null) || (to_price_raw.equals("")))
                    ? null : Double.parseDouble(to_price_raw);
            from = ((from_raw == null) || (from_raw.equals("")))
                    ? null : Date.valueOf(from_raw);
            to = ((to_raw == null) || (to_raw.equals("")))
                    ? null : Date.valueOf(to_raw);
            List<Product> products = d.search(key, from, to, price1, price2, cid);
                if ("1".equals(sort)) {
                Collections.sort(products, (Product o1, Product o2) -> o1.getPrice() > o2.getPrice() ? 1 : -1);
            } else if("-1".equals(sort)) {
                Collections.sort(products, (Product o1, Product o2) -> o1.getPrice() < o2.getPrice() ? 1 : -1);
            }
        
             
//phan trang
      
        
           

            //
            request.setAttribute("products", products);
            request.setAttribute("cid", cid);

        } catch (NumberFormatException e) {
        }
        HttpSession ss = request.getSession();
        if (ss.getAttribute("name") != null) {
            request.getRequestDispatcher("shopLogged.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("shop.jsp").forward(request, response);
        }

    }

    public static void main(String[] args) {
        int page = 2;

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
