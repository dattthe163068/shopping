/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cart;
import model.Item;
import model.Product;

/**
 *
 * @author trant
 */
@WebServlet(name = "removeitemServlet", urlPatterns = {"/remove"})
public class removeitemServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet removeitemServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet removeitemServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        String id_raw = request.getParameter("id");
        String txt = "";

        try {
            int id;
            id = Integer.parseInt(id_raw);
            DAO d = new DAO();
            List<Product> list;
            list = d.getALL();
            Cookie[] arr = request.getCookies();
            if (arr != null) {
                for (Cookie cookie : arr) {
                    if (cookie.getName().equals("cart")) {
                        txt += cookie.getValue();
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);

                    }
                }
                String txt1 = "";
//                Cart cart = new Cart(txt, list);
                if (txt.length() != 0 && null != txt) {
                    String[] str = txt.split("/");
                    for (int i = 0; i < str.length; i++) {

                        String[] n = str[i].split(":");
                        if (!n[0].equals(id_raw)) {
                            if (txt1.isEmpty()) {
                                txt1 = str[i];
                            } else {
                                txt1 += "/" + str[i];

                            }

                        }

                    }
                    if (!txt1.isEmpty()) {
                        Cookie cookie = new Cookie("cart", txt1);
                        cookie.setMaxAge(60 * 60 * 24 * 2);
                        response.addCookie(cookie);
                        Cart cart = new Cart(txt1, list);
                        request.setAttribute("cart", cart.getItems());
                    }

                }
//              
            }

            request.getRequestDispatcher("cart.jsp").forward(request, response);
        } catch (NumberFormatException e) {
//            Logger.getLogger(removeitemServlet.class.getName()).log(Level.SEVERE, "Invalid ID format", e);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
