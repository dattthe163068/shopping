/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import java.util.Random;
import model.Customer;
import model.Product;

/**
 *
 * @author trant
 */
@WebServlet(name = "loginServlet", urlPatterns = {"/login"})
public class loginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet loginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet loginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //        processRequest(request, response);
//        response.sendRedirect("login.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //        processRequest(request, response);

        String remember = request.getParameter("remember");

        String u = (String) request.getParameter("username");
        String p = (String) request.getParameter("password");
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("usernameC".equals(cookie.getName())) {
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }
        Cookie usernameCookie = new Cookie("usernameC", u);
        Cookie passwordCookie = new Cookie("passwordC", p);
        Cookie rememberCookie = new Cookie("rememberC", remember);
        if (remember != null) {
            rememberCookie.setMaxAge(60 * 60 * 24 * 7);
            usernameCookie.setMaxAge(60 * 60 * 24 * 7);
            usernameCookie.setMaxAge(60 * 60 * 24 * 7);
        } else {
            rememberCookie.setMaxAge(0);
            usernameCookie.setMaxAge(0);
            passwordCookie.setMaxAge(0);
        }
        //luu cookie vaof browser
        response.addCookie(rememberCookie);
        response.addCookie(usernameCookie);
        response.addCookie(passwordCookie);

        DAO d = new DAO();
        Customer a = d.getAccount(u, p);
        //
        Random random = new Random();
        int randomNumber = random.nextInt(4) + 1;
        String input = "";
        switch (randomNumber) {
            case 1:
                input += "IPHONE";
                break;
            case 2:
                input += "SAMSUNG";
                break;
            case 3:
                input += "OPPO";
                break;
            case 4:
                input += "VSMART";
                break;
            default:
                break;
        }
        List<Product> products = d.RandomProduct(input);

        if (a == null) {
            request.setAttribute("error", "tai khoan ko ton tai");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("account", a);
            request.setAttribute("username", u);
            request.setAttribute("product", products);
            session.setAttribute("name", u);

            request.getRequestDispatcher("homeLogged.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void main(String[] args) {
        DAO d = new DAO();
        Customer a = d.getAccount("manh", "123");
        System.out.println(a.getName());
    }
}
