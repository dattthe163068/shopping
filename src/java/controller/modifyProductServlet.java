/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import model.Product;
import model.ProductView;

/**
 *
 * @author trant
 */
@WebServlet(name = "modifyProductServlet", urlPatterns = {"/modifyProduct"})
public class modifyProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet modifyProductServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet modifyProductServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        String id_raw = request.getParameter("id");
        try {
            int id;
            id = Integer.parseInt(id_raw);
            DAO d = new DAO();
            ProductView p = new ProductView();
            p = d.getProductViewById(id);
            
            request.setAttribute("product", p);
            
            request.getRequestDispatcher("modifyProduct.jsp").forward(request, response);


        } catch (IOException | NumberFormatException e) {
            
        }
    }
    
 

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        String id_raw = request.getParameter("id");
        String name = request.getParameter("name");
        String price_raw = request.getParameter("price");
        String quantity_raw = request.getParameter("quantity");
        String image = request.getParameter("image");
        
        String releasedate = request.getParameter("date");
        String describe = request.getParameter("describe");
        String cid_raw = request.getParameter("cid");
        String RAM_raw = request.getParameter("ram");
        String CPU = request.getParameter("cpu");
        String GPU = request.getParameter("gpu");
        String Display = request.getParameter("display");
        PrintWriter out =response.getWriter();
     
        DAO d = new DAO();
        int id;
        int quantity;
        double price;
        Date date;
        int cid;
        int RAM;
        
        try {
        id=Integer.parseInt(id_raw);
        quantity=Integer.parseInt(quantity_raw);
        price=Double.parseDouble(price_raw);
        date=Date.valueOf(releasedate);
        cid=Integer.parseInt(cid_raw);
        RAM=Integer.parseInt(RAM_raw);
        
            d.modifyProduct(id, name, price, quantity, image, date, describe, cid, RAM, CPU, GPU, Display);
            response.sendRedirect("listCRUD");
            
        } catch (IOException | NumberFormatException e) {
            out.print(e);
        }
        
    }
    public static void main(String[] args) {
         String dateString = "2023-07-19";
        Date date = Date.valueOf(dateString);
      

        // Tạo đối tượng ngày từ timestamp (milliseconds)
        long timestamp = System.currentTimeMillis();
        Date dateFromTimestamp = new Date(timestamp);
         DAO d = new DAO();
         d.modifyProduct(34, "dfdfdfdfdf", 23.2, 2356, "rrrr", dateFromTimestamp, "tttt", 2, 12, "dfd", "dfd", "df");
        Product p= d.getProductById(34);
         System.out.println(p.getQuantity());
         
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
