/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Cart;
import model.Item;
import model.Product;

/**
 *
 * @author trant
 */
@WebServlet(name = "proccessServlet", urlPatterns = {"/proccess"})
public class proccessServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter Cartout = response.getWriter()) {
            /* TODO Cartoutput your page here. You may use following sample code. */
            Cartout.println("<!DOCTYPE html>");
            Cartout.println("<html>");
            Cartout.println("<head>");
            Cartout.println("<title>Servlet proccessServlet</title>");
            Cartout.println("</head>");
            Cartout.println("<body>");
            Cartout.println("<h1>Servlet proccessServlet at " + request.getContextPath() + "</h1>");
            Cartout.println("</body>");
            Cartout.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        String id_raw = (String) request.getParameter("id");
        String num_raw = (String) request.getParameter("num");
        DAO d = new DAO();
        List<Product> list;
        list = d.getALL();
        Cookie[] arr = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie cookie : arr) {
                if (cookie.getName().equals("cart")) {
                    txt += cookie.getValue();
                }
            }
            Cart cart = new Cart(txt, list);

            try {
                int id;
                int num=0;
                num = Integer.parseInt(num_raw);
                id = Integer.parseInt(id_raw);

                Product p = d.getProductById(id);
                int numStock = p.getQuantity();

                if (num == -1 && (cart.getQuantityById(id) <= 1)) {
                    cart.removeItemById(id);
                } else if (num == 1 && cart.getQuantityById(id) >= numStock) {
                    num = 0;
                } else {
                    double price = p.getPrice();
                    Item t = new Item(p, num, price);
                    cart.addItem(t);
                }
            } catch (NumberFormatException e) {
            }
            List<Item> items = cart.getItems();

            if (!items.isEmpty()) {
                txt = items.get(0).getProduct().getId() + ":" + items.get(0).getQuantity();
                for (int i = 1; i < items.size(); i++) {
                    txt += "/" + items.get(i).getProduct().getId() + ":" + items.get(i).getQuantity();

                }
            } else {
                txt = "";
            }
            Cookie c = new Cookie("cart", txt);
            c.setMaxAge(2 * 24 * 60 * 60);
            response.addCookie(c);
            request.setAttribute("cart", cart.getItems());
                   HttpSession ss= request.getSession();

              ss.setAttribute("size", items.size());
            request.getRequestDispatcher("cart.jsp").forward(request, response);
        }

    }

    public static void main(String[] args) {
        String txt = "1:1";
        DAO d = new DAO();
        List<Product> list;
        list = d.getALL();
        Cart cart = new Cart(txt, list);

        int id = 1;
        int num = -1;
        Product p = d.getProductById(id);
        int numStock = p.getQuantity();
        if (num == -1 && (cart.getQuantityById(id) <= 1)) {
            cart.removeItemById(id);
        } else if (num == 1 && cart.getQuantityById(id) >= numStock) {
            num = 0;
        } else {
            double price = p.getPrice();
            Item t = new Item(p, num, price);
            cart.addItem(t);
        }
        List<Item> items = cart.getItems();

        if (!items.isEmpty()) {
            txt = items.get(0).getProduct().getId() + ":" + items.get(0).getQuantity();
            for (int i = 1; i < items.size(); i++) {
                txt += "/" + items.get(i).getProduct().getId() + ":" + items.get(i).getQuantity();

            }
        }

        System.out.println(txt);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        String id = request.getParameter("id");

        DAO d = new DAO();
        List<Product> list;
        list = d.getALL();
        Cookie[] arr = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie cookie : arr) {
                if (cookie.getName().equals("cart")) {
                    txt += cookie.getValue();
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }

        }

        String[] ids = txt.split("/");
        //new string to be cookie after remove one order
        String Cartout = "";
        for (int i = 0; i < ids.length; i++) {
            String[] s = ids[i].split(":");
            if (!s[0].equals(id)) {
                if (Cartout.isEmpty()) {
                    Cartout = ids[i];

                } else {
                    Cartout += "/" + ids[i];
                }
            }
        }
        if (!Cartout.isEmpty()) {
            Cookie c = new Cookie("cart", Cartout);
            c.setMaxAge(2 * 24 * 60 * 60);
            response.addCookie(c);
        }
        Cart cart = new Cart(Cartout, list);
        request.setAttribute("cart", cart.getItems());
        request.getRequestDispatcher("mycart.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
