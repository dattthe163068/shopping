import controller.signUpTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({signUpTest.class})
public class TestSuite {
    // This class doesn't contain any code, it just acts as a test suite.
    // You can add more test classes to the @SuiteClasses annotation if needed.
    // If all test cases pass, you can display a success message here.
    public static void main(String[] args) {
        System.out.println("success");
    }
}
