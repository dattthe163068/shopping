/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package dal;

import java.util.List;
import model.Category;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class CategoryDAOTest {
    
    public CategoryDAOTest() {
    }

    @Test
    public void testGetALL() {
          CategoryDAO c = new CategoryDAO();
        List<Category> list = c.getALL();
        assertEquals("Đẹp rực rỡ rạng ngời", list.get(0).getDescribe());
    }

    @Test
    public void testDelete() {
    }

    @Test
    public void testGetCategoryById() {
    }

    @Test
    public void testInsert() {
    }

    @Test
    public void testUpdate() {
    }

    @Test
    public void testMain() {
    }
    
}
