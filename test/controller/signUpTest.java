/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import static controller.signupServlet.EMAIL_REGEX;
import static controller.signupServlet.NAME_REGEX;
import static controller.signupServlet.PHONE_REGEX;
import static controller.signupServlet.USERNAME_PASSWORD_REGEX;
import dal.DAO;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.Customer;
import model.CustomerDetail;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Admin
 */
public class signUpTest  {

    @Test
    public void nameInputTest() {
        signupServlet ss = new signupServlet();
        String name = "tran thanh congd";
        boolean actualResult = ss.validateInput(name, signupServlet.NAME_REGEX);
        if (actualResult == false) {
            Assert.fail("name format not matched");
        }
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");

    }

    @Test
    public void usernameInputTest() {
        signupServlet ss = new signupServlet();
        String username = "asdad";
        boolean actualResult = ss.validateInput(username, signupServlet.USERNAME_PASSWORD_REGEX);

        if (actualResult == false) {
            Assert.fail("username format not matched");
        }
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");

    }

    @Test
    public void usernameDupTest() {
        String username = "asdad";
        DAO d = new DAO();
        boolean actualResult = d.checkDuplicateUserName(username);
        if (actualResult == true) {
            Assert.fail("username duplicate");
        }
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");

    }

    @Test
    public void passwordInputTest() {
        signupServlet ss = new signupServlet();
        String password = "deptrai";
        boolean actualResult = ss.validateInput(password, signupServlet.USERNAME_PASSWORD_REGEX);
        if (actualResult == false) {
            Assert.fail("password format not matched");
        }
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");
    }

    @Test
    public void emailInputTest() {
        signupServlet ss = new signupServlet();
        String email = "dattran1234@gmail.com";
        boolean actualResult = ss.validateInput(email, signupServlet.EMAIL_REGEX);
        if (actualResult == false) {
            Assert.fail("email format not matched");
        }
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");
    }

    @Test
    public void phoneInputTest() {
        signupServlet ss = new signupServlet();
        //
        String phone = "0912900963";
        //
        boolean actualResult = ss.validateInput(phone, signupServlet.PHONE_REGEX);
        if (actualResult == false) {
            Assert.fail("phone format not matched");
        }
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");
    }

    @Test
    public void countryInputTest() {
        signupServlet ss = new signupServlet();
        String country = "italy";

        boolean actualResult = ss.checkNullCountry(country);
        if (actualResult == true) {
            Assert.fail("country should not be null");
        }
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, actualResult);
        System.out.println("success");
    }

    @Test

    public void addUserTest() {

        signupServlet ss = new signupServlet();

        String name = "tran thanh congd";
        String username = "asdad";
        String password = "deptrai";
        String email = "dattran1234@gmail.com";
        String phone = "0912900963";
        String country = "italy";
        boolean isNameValid = ss.validateInput(name, NAME_REGEX);
        boolean isUsernameValid = ss.validateInput(username, USERNAME_PASSWORD_REGEX);
        boolean isPasswordValid = ss.validateInput(password, USERNAME_PASSWORD_REGEX);
        boolean isEmailValid = ss.validateInput(email, EMAIL_REGEX);
        boolean isPhoneValid = ss.validateInput(phone, PHONE_REGEX);
        DAO d = new DAO();
        if (d.checkDuplicateUserName(username) == false
                && ss.checkNullCountry(country) == false
                && isNameValid && isUsernameValid
                && isPasswordValid
                && isEmailValid && isPhoneValid) {
            d.addUser(name, username, password, email, phone, country);
            try {
                Customer c = d.getCustomerByUserName(username);
                CustomerDetail cd = d.getCustomerDetailById(c.getId());
                System.out.println(c.toString());
                System.out.println(cd.toString());
            } catch (NullPointerException e) {
                Assert.fail("test failed due to null value ");
            }

        } else {

            Assert.fail("cannot execute database sql,failed");
        }

    }

}
