/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dal.DAO;
import java.sql.Date;
import java.util.List;
import model.Product;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Admin
 */
public class SearchTest {
    
    @Test
    public void searchByPriceTest() {
        double price1 = 40000;
        double price2 = 30000000;
        //
        DAO d = new DAO();
        List<Product> list = d.search(null, null, null, price1, price2, 0);
        for (Product product : list) {
            if (product.getPrice() < 40000 && product.getPrice() > 30000000) {
                Assert.fail("test failed due to price out of range");
            }
        }
        
    }
    
    @Test
    public void searchByDateTest() {
        String dateString1 = "2021-10-10";
        String dateString2 = "2023-10-10";
        
        Date from = Date.valueOf(dateString1);
        java.util.Date utilDateFrom = new java.util.Date(from.getTime());
        Date to = Date.valueOf(dateString2);
        java.util.Date utilDateTo = new java.util.Date(to.getTime());
        
        DAO d = new DAO();
        List<Product> list = d.search(null, from, to, null, null, 0);
        
        for (Product product : list) {
            Date releaseDate = (Date) product.getReleaseDate();

            // Compare releaseDate with utilDateFrom and utilDateTo using compareTo
            if (releaseDate.compareTo(utilDateFrom) < 0 || releaseDate.compareTo(utilDateTo) > 0) {
                Assert.fail("test failed due to release date out of range");
            }
        }
    }

    @Test
    public void searchByNameTest() {
        String key = "oppo";
        DAO d = new DAO();
        List<Product> list = d.search(key, null, null, null, null, 0);
        for (Product product : list) {
            if (!product.getName().startsWith("Oppo")) {
                Assert.fail("test failed due to unexpected product in output");
            }
            
        }
    }
    
}
