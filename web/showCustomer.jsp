<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
    <style>
        /* Style the table */
        table {
            border-collapse: collapse;
            width: 100%;
        }

        /* Style the table header cells */
        th {
            background-color: #f2f2f2;
            text-align: left;
            padding: 8px;
        }

        /* Style the table data cells */
        td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        /* Style the table data rows alternately for better readability */
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        /* Add some extra styling for the first column (ID) */
        td:first-child {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <a href="dash.jsp">Back to Admin Page</a>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>AMOUNT</th>
                <th>USERNAME</th>
                <th>PASSWORD</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="c" items="${requestScope.customers}">
                <tr>
                    <td>${c.id}</td>
                    <td>${c.name}</td>
                    <td>${c.amount}</td>
                    <td>${c.customername}</td>
                    <td>${c.password}
                        <a style="float: right" href="setRole?id=${c.id}">Role</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
