<%-- 
    Document   : shop
    Created on : Jul 6, 2023, 3:23:30 AM
    Author     : trant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="cookie" value="${pageContext.request.cookies}"  />

<!DOCTYPE html>
<html>
    <head>
        <style>
            .range-form {
                width: 400px;
                margin: auto;
                padding: 50px;
            }

            .range-slider {
                -webkit-appearance: none;
                /* Override default CSS styles */
                appearance: none;
                width: 100%;
                /* Full-width */
                height: 10px;
                border-radius: 5px;
                /* Specified height */
                background: #d3d3d3;
                /* Grey background */
                outline: none;
                /* Remove outline */
                opacity: 0.7;
                /* Set transparency (for mouse-over effects on hover) */
                -webkit-transition: .2s;
                /* 0.2 seconds transition on hover */
                transition: opacity .2s;
                &:hover {
                    opacity: 1;
                    /* Fully shown on mouse-over */
                }
                &::-webkit-slider-thumb {
                    -webkit-appearance: none;
                    /* Override default look */
                    appearance: none;
                    width: 18px;
                    /* Set a specific slider handle width */
                    height: 18px;
                    /* Slider handle height */
                    background: #4CAF50;
                    /* Green background */
                    cursor: pointer;
                    /* Cursor on hover */
                    border-radius: 50%;
                }
                &::-moz-range-thumb {
                    width: 18px;
                    /* Set a specific slider handle width */
                    height: 18px;
                    /* Slider handle height */
                    background: #4CAF50;
                    /* Green background */
                    cursor: pointer;
                    /* Cursor on hover */
                }
            }

        </style>
        <script src="https://kit.fontawesome.com/d662e7eda2.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


        <script>
            // Get the input elements and span elements
            var fromPriceInput = document.getElementsByName("fromprice")[0];
            var toPriceInput = document.getElementsByName("toprice")[0];
            var fromPriceOutput = document.getElementById("demo");
            var toPriceOutput = document.getElementById("demo1");

            // Function to update the span values
            function updateSpanValues() {
                fromPriceOutput.textContent = fromPriceInput.value;
                toPriceOutput.textContent = toPriceInput.value;
            }

            // Update the span values initially
            updateSpanValues();

            // Add event listeners to the input sliders
            fromPriceInput.addEventListener("input", updateSpanValues);
            toPriceInput.addEventListener("input", updateSpanValues);
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Boutique | Ecommerce bootstrap template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- gLightbox gallery-->
        <link rel="stylesheet" href="vendor/glightbox/css/glightbox.min.css">
        <!-- Range slider-->
        <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css">
        <!-- Choices CSS-->
        <link rel="stylesheet" href="vendor/choices.js/public/assets/styles/choices.min.css">
        <!-- Swiper slider-->
        <link rel="stylesheet" href="vendor/swiper/swiper-bundle.min.css">
        <!-- Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.png">
        <style>
            .hoverDat:hover{
                transform: scale(150%);

            }

        </style>
        <script type="text/javascript">
            var change = function () {
                document.getElementById("f1").submit();

            }
            var change1 = function () {
                document.getElementById("f2").submit();

            }
        </script>
        <script>
            // Gọi hàm datepicker() cho các trường nhập ngày
            $(document).ready(function () {
                $('input[type="date"]').datepicker({
                    format: 'yyyy-mm-dd', // Định dạng ngày tháng
                    autoclose: true, // Tự động đóng picker sau khi chọn ngày
                    todayHighlight: true // Làm nổi bật ngày hiện tại
                });
            });
        </script>

    </head>
    <body>
        <div class="page-holder">

            <!-- navbar-->
            <header class="header bg-white">
                <div class="container px-lg-3">
                    <nav class="navbar navbar-expand-lg navbar-light py-3 px-lg-0"><a class="navbar-brand" href="homeLogged.jsp"><span class="fw-bold text-uppercase text-dark">Boutique</span></a>
                        <button class="navbar-toggler navbar-toggler-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto">
                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link" href="homeLogged.jsp">Home</a>
                                </li>
                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link active" href="shop.html">Shop</a> 

                                </li>

                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link" href="productview?id=${p.id}">Product detail</a>
                                </li>
                                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" id="pagesDropdown" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                                    <div class="dropdown-menu mt-3 shadow-sm" aria-labelledby="pagesDropdown"><a class="dropdown-item border-0 transition-link" href="homeLogged.jsp">Homepage</a><a class="dropdown-item border-0 transition-link" href="shop.html">Category</a><a class="dropdown-item border-0 transition-link" href="productview?id=${p.id}">Product detail</a><a class="dropdown-item border-0 transition-link" href="show">Shopping cart</a><a class="dropdown-item border-0 transition-link" href="checkout.html">Checkout</a></div>
                                </li>
                            </ul>
                            <ul class="navbar-nav ms-auto">               
                                <li class="nav-item"><a class="nav-link" href="show"> <i class="fas fa-dolly-flatbed me-1 text-gray"></i>Cart<small class="text-gray fw-normal">(${sessionScope.size})</small></a></li>
                                <li class="nav-item"><a class="nav-link" href="#!"> <i class="far fa-heart me-1"></i><small class="text-gray fw-normal"> (0)</small></a></li>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i style="margin-right: 9px "  class="fa-solid fa-user" style="color: #ffffff;"></i>${sessionScope.name}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="account?name=${sessionScope.name}">User Profile</a>
                                        <a class="dropdown-item" href="logout">Log Out</a>
                                        <a class="dropdown-item" href="authorize">Account Setting</a>

                                    </div>
                                </div>
                            </ul>
                        </div>
                    </nav>
                </div>
            </header>
            <!--  Modal -->
            <div class="modal fade" id="productview?id=${p.id}?id=${p.id}" tabindex="-1">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content overflow-hidden border-0">
                        <button class="btn-close p-4 position-absolute top-0 end-0 z-index-20 shadow-0" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="modal-body p-0">
                            <div class="row align-items-stretch">
                                <div class="col-lg-6 p-lg-0"><a class="glightbox product-view d-block h-100 bg-cover bg-center" style="background: url(img/product-5.jpg)" href="img/product-5.jpg" data-gallery="gallery1" data-glightbox="Red digital smartwatch"></a><a class="glightbox d-none" href="img/product-5-alt-1.jpg" data-gallery="gallery1" data-glightbox="Red digital smartwatch"></a><a class="glightbox d-none" href="img/product-5-alt-2.jpg" data-gallery="gallery1" data-glightbox="Red digital smartwatch"></a></div>
                                <div class="col-lg-6">
                                    <div class="p-4 my-md-4">
                                        <ul class="list-inline mb-2">
                                            <li class="list-inline-item m-0"><i class="fas fa-star small text-warning"></i></li>
                                            <li class="list-inline-item m-0 1"><i class="fas fa-star small text-warning"></i></li>
                                            <li class="list-inline-item m-0 2"><i class="fas fa-star small text-warning"></i></li>
                                            <li class="list-inline-item m-0 3"><i class="fas fa-star small text-warning"></i></li>
                                            <li class="list-inline-item m-0 4"><i class="fas fa-star small text-warning"></i></li>
                                        </ul>
                                        <h2 class="h4">Red digital smartwatch</h2>
                                        <p class="text-muted">$250</p>
                                        <p class="text-sm mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.</p>
                                        <div class="row align-items-stretch mb-4 gx-0">
                                            <div class="col-sm-7">
                                                <div class="border d-flex align-items-center justify-content-between py-1 px-3"><span class="small text-uppercase text-gray mr-4 no-select">Quantity</span>
                                                    <div class="quantity">
                                                        <button class="dec-btn p-0"><i class="fas fa-caret-left"></i></button>
                                                        <input class="form-control border-0 shadow-0 p-0" type="text" value="1">
                                                        <button class="inc-btn p-0"><i class="fas fa-caret-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5"><a class="btn btn-dark btn-sm w-100 h-100 d-flex align-items-center justify-content-center px-0" href="show">Add to cart</a></div>
                                        </div><a class="btn btn-link text-dark text-decoration-none p-0" href="#!"><i class="far fa-heart me-2"></i>Add to wish list</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- HERO SECTION-->
                <section class="py-5 bg-light">
                    <div class="container">
                        <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                            <div class="col-lg-6">
                                <h1 class="h2 text-uppercase mb-0">Shop</h1>
                                <form action="list" >
                                    <div class="input-group rounded">
                                        <input name="key2" type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                        <input class="btn btn-primary" type="submit" value="Search">



                                        </span>
                                    </div>
                                </form>

                            </div>
                            <div class="col-lg-6 text-lg-end">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-lg-end mb-0 px-0 bg-light">
                                        <li class="breadcrumb-item"><a class="text-dark" href="homeLogged.jsp">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Shop</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="py-5">
                    <div class="container p-0">
                        <div class="row">
                            <!-- SHOP SIDEBAR-->
                            <div class="col-lg-3 order-2 order-lg-1">
                                <h5 class="text-uppercase mb-4">Categories</h5>
                                <div class="py-2 px-4 bg-dark text-white mb-3"><strong class="small text-uppercase fw-bold">Categories &amp; Search</strong></div>
                                <ul class="list-unstyled small text-muted ps-lg-4 font-weight-normal">
                                    <li class="mb-2"><a class="reset-anchor" href="#!">IPHONE</a></li>
                                    <li class="mb-2"><a class="reset-anchor" href="#!">SAMSUNG</a></li>
                                    <li class="mb-2"><a class="reset-anchor" href="#!">OPPO</a></li>
                                    <li class="mb-2"><a class="reset-anchor" href="#!">VSMART</a></li>

                                </ul>


                                <h6 class="text-uppercase mb-4">Search By Price</h6>
                                <form id="f2" action="list">


                                    <div class="form-group row">


                                        <div class="input-group mb-3">
                                            <input name="fromprice" type="number" class="form-control" placeholder="From" aria-label="Username">
                                            <span class="input-group-text"><i class="fa-solid fa-arrow-right"></i></span>
                                            <input name="toprice" type="number" class="form-control" placeholder="To" aria-label="Server">
                                        </div>


                                    </div>
                                    <button type="submit" class="btn btn-light">Search</button>

                                    <br>

                                    <br>
                                    <br>

                                    <h6 class="text-uppercase mb-4">Search By Released Date</h6>
                                    <div style="color:brown" class="mb-2"><a class="reset-anchor" href="#!">From Date</a></div>
                                    <input type="date" name="fromdate" class="datepicker">
                                    <br>
                                    <br>
                                    <div style="color:brown" lass="mb-2"> <a class="reset-anchor" href="#!">To Date</a></div>
                                    <input type="date" name="todate" class="datepicker"> <br> <br>


                                    <button type="submit" class="btn btn-dark">Search</button>

                                </form>

                                <br>
                                <form name="sort" action="sort" method="get">
                                    <h6 class="text-uppercase mb-3">Sort by Price</h6>
                                    <div class="form-check mb-1">
                                        <input onclick="submit()" class="form-check-input" type="radio" name="customRadio" value="1" id="radio_1">
                                        <label class="form-check-label" for="radio_1">Low to High</label>
                                    </div>
                                    <div class="form-check mb-1">
                                        <input onclick="submit()" class="form-check-input" type="radio" name="customRadio" value="-1" id="radio_2">
                                        <label class="form-check-label" for="radio_2"> High to Low</label>
                                    </div>
                                </form>
                            </div>
                            <!-- SHOP LISTING-->

                            <div class="col-lg-9 order-1 order-lg-2 mb-5 mb-lg-0">
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-6 mb-2 mb-lg-0">
                                        <p class="text-sm text-muted mb-0">Showing 1–6 of 53 results</p>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="list-inline d-flex align-items-center justify-content-lg-end mb-0">
                                            <li class="list-inline-item text-muted me-3"><a class="reset-anchor p-0" href="#!"><i class="fas fa-th-large"></i></a></li>
                                            <li class="list-inline-item text-muted me-3"><a class="reset-anchor p-0" href="#!"><i class="fas fa-th"></i></a></li>
                                            <li class="list-inline-item">
                                                <c:set var="cid" value="${requestScope.cid}" />
                                                <form action="list" id="f1">

                                                    <select name="key" onchange="change()" class="selectpicker" data-customclass="form-control form-control-sm">
                                                        <option value="0">ALL </option>
                                                        <c:forEach items="${requestScope.data}" var="c">
                                                            <option ${(cid==c.id)?'selected':''} value="${c.id}">${c.name}</option>

                                                        </c:forEach>
                                                    </select>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">


                                    <!-- PRODUCT-->
                                    <c:forEach items="${requestScope.products}" var="p">
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="product text-center">
                                                <div class="mb-3 position-relative">
                                                    <div class="badge text-white bg-"></div><a class="d-block" href="productview?id=${p.id}"><img class="img-fluid w-100" src="${p.image}" alt="..."></a>
                                                    <div class="product-overlay">
                                                        <ul class="mb-0 list-inline">
                                                            <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-outline-dark" href="#!"><i class="far fa-heart"></i></a></li>
                                                            <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="productview?id=${p.id}">View</a></li>  
                                                            <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="productview?id=${p.id}?id=${p.id}" data-bs-toggle="modal"><i class="fas fa-expand"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <h6> <a class="reset-anchor" href="productview?id=${p.id}">${p.name}</a></h6>
                                                    <fmt:formatNumber pattern="#,##0VND" value="${p.price}"></fmt:formatNumber>
                                                </div>
                                            </div>
                                    </c:forEach>

                                    <!-- PAGINATION-->
                                    <nav aria-label="Page navigation example">
                                        <c:set value="${requestScope.page}" var="page"  />
                                        <ul class="pagination justify-content-center justify-content-lg-end">
                                            <li class="page-item mx-1"><a class="page-link" href="#!" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                                                <c:forEach begin="${1}" end="${requestScope.num}" var="i"  >
                                                <li   " class="page-item mx-1 active"><a style="transition:scale 2s ease-in  " class="page-link hoverDat" href="list?page=${i}">${i}</a></li>
                                                <!--<li   " class="page-item mx-1 active"><a style="transition:scale 2s ease-in  " class="page-link hoverDat" href="list?page=${i}">${i}</a></li>-->

                                            </c:forEach>
                                            <li class="page-item ms-1"><a class="page-link" href="#!" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                </section>
            </div>
            <footer class="bg-dark text-white">
                <div class="container py-4">
                    <div class="row py-5">
                        <div class="col-md-4 mb-3 mb-md-0">
                            <h6 class="text-uppercase mb-3">Customer services</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a class="footer-link" href="#!">Help &amp; Contact Us</a></li>
                                <li><a class="footer-link" href="#!">Returns &amp; Refunds</a></li>
                                <li><a class="footer-link" href="#!">Online Stores</a></li>
                                <li><a class="footer-link" href="#!">Terms &amp; Conditions</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 mb-3 mb-md-0">
                            <h6 class="text-uppercase mb-3">Company</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a class="footer-link" href="#!">What We Do</a></li>
                                <li><a class="footer-link" href="#!">Available Services</a></li>
                                <li><a class="footer-link" href="#!">Latest Posts</a></li>
                                <li><a class="footer-link" href="#!">FAQs</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h6 class="text-uppercase mb-3">Social media</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a class="footer-link" href="#!">Twitter</a></li>
                                <li><a class="footer-link" href="#!">Instagram</a></li>
                                <li><a class="footer-link" href="#!">Tumblr</a></li>
                                <li><a class="footer-link" href="#!">Pinterest</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="border-top pt-4" style="border-color: #1d1d1d !important">
                        <div class="row">
                            <div class="col-md-6 text-center text-md-start">
                                <p class="small text-muted mb-0">&copy; 2021 All rights reserved.</p>
                            </div>
                            <div class="col-md-6 text-center text-md-end">
                                <p class="small text-muted mb-0">Template designed by <a class="text-white reset-anchor" href="https://bootstrapious.com/p/boutique-bootstrap-e-commerce-template">Bootstrapious</a></p>
                                <!-- If you want to remove the backlink, please purchase the Attribution-Free License. See details in readme.txt or license.txt. Thanks!-->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- JavaScript files-->
            <script>
                var submit = function () {
                    documents.sort.submit();

                }

            </script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="vendor/glightbox/js/glightbox.min.js"></script>
            <script src="vendor/nouislider/nouislider.min.js"></script>
            <script src="vendor/swiper/swiper-bundle.min.js"></script>
            <script src="vendor/choices.js/public/assets/scripts/choices.min.js"></script>
            <script src="js/front.js"></script>
            <!-- Nouislider Config-->

            <script>
                // ------------------------------------------------------- //
                //   Inject SVG Sprite - 
                //   see more here 
                //   https://css-tricks.com/ajaxing-svg-sprite/
                // ------------------------------------------------------ //
                function injectSvgSprite(path) {

                    var ajax = new XMLHttpRequest();
                    ajax.open("GET", path, true);
                    ajax.send();
                    ajax.onload = function (e) {
                        var div = document.createElement("div");
                        div.className = 'd-none';
                        div.innerHTML = ajax.responseText;
                        document.body.insertBefore(div, document.body.childNodes[0]);
                    }
                }
                // this is set to BootstrapTemple website as you cannot 
                // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
                // while using file:// protocol
                // pls don't forget to change to your domain :)
                injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');

            </script>
            <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
