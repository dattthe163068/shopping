<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : OrderHistory
    Created on : Jul 19, 2023, 6:53:51 PM
    Author     : trant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Order History</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 20px;
            }

            h1 {
                text-align: center;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th, td {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            th {
                background-color: #f2f2f2;
            }

            tr:nth-child(even) {
                background-color: #f2f2f2;
            }

        </style>

    </head>
    <body>
          <a href="dash.jsp">Go back to homepage</a>
        <h1>Order History</h1>
        <table id="order-table">
            <tr>
                <th>Order ID</th>
                <th>Customer ID</th>
                <th>Date</th>
                <th>Total Money</th>
            </tr>
            <c:forEach var="o" items="${requestScope.order}">
                <tr>
                    <td>${o.id}</td>
                    <td>${o.cid}</td>
                    <td>${o.date}</td>
                    <td> <fmt:formatNumber pattern="#,##0VND" value="${o.totalMoney}"></fmt:formatNumber>
                        <a style="float:right" href="showorderline?oid=${o.id}">Detail</a>
                        </td>
                    </tr>
            </c:forEach>

        </table>
        <script src="script.js"></script>
    </body>
</html>
