<%-- Document : myshop Created on : Jul 3, 2023, 11:03:50 PM Author : trant --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

                <!DOCTYPE html>
                <html>

                <head>
                    <script src="https://kit.fontawesome.com/d662e7eda2.js" crossorigin="anonymous"></script>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>myshop Page</title>
                    <style>
                        table {

                            border-collapse: collapse;
                        }

                        #bag {

                            text-align: center;
                            margin-right: 30px;
                            margin-top: 30px;

                        }

                        .tr {
                            text-align: right;
                        }
                    </style>
                </head>

                <body>
                    <p id="bag">
                        <i style="width: 50px; height: 50px ;" class="fa-solid fa-cart-shopping"></i>
                          <a href="show">my bag(${requestScope.size})</a>
                    </p>
                    <h1>list produce!</h1>
                    <form method="post" name="f" action="">

                        enter number of items to buy
                        <input style="text-align: center;" type="number" name="num" value="1">
                        <br>
                        </form>
                        <table border="1px " width="40%">
                            <tr>
                                <th>ID</th>
                                <th>name</th>
                                <th>quantity</th>
                                <th>Price</th>
                                <th>action</th>
                                

                            </tr>
                            <c:forEach items="${requestScope.data}" var="p">
                               
                                <tr>
                                    <td>${p.id}</td>
                                    <td>${p.name}</td>
                                    <td class="tr">${p.quantity}</td>
                                    <td class="tr">
                                        <fmt:formatNumber pattern="##.#" value="${p.price*2}" />
                                    </td>
                                    <td><input type="button" onclick="buy('${p.id}')"  value="buy item"> </td>


                                </tr>
                            </c:forEach>
                        </table>
                    
                </body>

                </html>
                <script>
                    var buy = function (id) {
                        var m = document.f.num.value;
                        document.f.action = "buy?id="+id+"&num="+m;
                        document.f.submit();
                    }
                </script>