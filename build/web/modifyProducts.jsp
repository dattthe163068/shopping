<%-- 
    Document   : shop
    Created on : Jul 6, 2023, 3:23:30 AM
    Author     : trant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="cookie" value="${pageContext.request.cookies}"  />

<!DOCTYPE html>
<html>
    <head>
        
        <style>
            .range-form {
                width: 400px;
                margin: auto;
                padding: 50px;
            }

            .range-slider {
                -webkit-appearance: none;
                /* Override default CSS styles */
                appearance: none;
                width: 100%;
                /* Full-width */
                height: 10px;
                border-radius: 5px;
                /* Specified height */
                background: #d3d3d3;
                /* Grey background */
                outline: none;
                /* Remove outline */
                opacity: 0.7;
                /* Set transparency (for mouse-over effects on hover) */
                -webkit-transition: .2s;
                /* 0.2 seconds transition on hover */
                transition: opacity .2s;
                &:hover {
                    opacity: 1;
                    /* Fully shown on mouse-over */
                }
                &::-webkit-slider-thumb {
                    -webkit-appearance: none;
                    /* Override default look */
                    appearance: none;
                    width: 18px;
                    /* Set a specific slider handle width */
                    height: 18px;
                    /* Slider handle height */
                    background: #4CAF50;
                    /* Green background */
                    cursor: pointer;
                    /* Cursor on hover */
                    border-radius: 50%;
                }
                &::-moz-range-thumb {
                    width: 18px;
                    /* Set a specific slider handle width */
                    height: 18px;
                    /* Slider handle height */
                    background: #4CAF50;
                    /* Green background */
                    cursor: pointer;
                    /* Cursor on hover */
                }
            }

        </style>
        <script src="https://kit.fontawesome.com/d662e7eda2.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


        <script>
            // Get the input elements and span elements
            var fromPriceInput = document.getElementsByName("fromprice")[0];
            var toPriceInput = document.getElementsByName("toprice")[0];
            var fromPriceOutput = document.getElementById("demo");
            var toPriceOutput = document.getElementById("demo1");

            // Function to update the span values
            function updateSpanValues() {
                fromPriceOutput.textContent = fromPriceInput.value;
                toPriceOutput.textContent = toPriceInput.value;
            }

            // Update the span values initially
            updateSpanValues();

            // Add event listeners to the input sliders
            fromPriceInput.addEventListener("input", updateSpanValues);
            toPriceInput.addEventListener("input", updateSpanValues);
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Boutique | Ecommerce bootstrap template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- gLightbox gallery-->
        <link rel="stylesheet" href="vendor/glightbox/css/glightbox.min.css">
        <!-- Range slider-->
        <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css">
        <!-- Choices CSS-->
        <link rel="stylesheet" href="vendor/choices.js/public/assets/styles/choices.min.css">
        <!-- Swiper slider-->
        <link rel="stylesheet" href="vendor/swiper/swiper-bundle.min.css">
        <!-- Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.png">
        <style>
            .hoverDat:hover{
                transform: scale(150%);

            }

        </style>
        <script type="text/javascript">
            var change = function () {
                document.getElementById("f1").submit();

            }
            var change1 = function () {
                document.getElementById("f2").submit();

            }
        </script>
        <script>
            // Gọi hàm datepicker() cho các trường nhập ngày
            $(document).ready(function () {
                $('input[type="date"]').datepicker({
                    format: 'yyyy-mm-dd', // Định dạng ngày tháng
                    autoclose: true, // Tự động đóng picker sau khi chọn ngày
                    todayHighlight: true // Làm nổi bật ngày hiện tại
                });
            });
        </script>

    </head>
    <body>
        <div class="page-holder">

         
         
            <div class="container">
                <!-- HERO SECTION-->
                                                       <a class="btn btn-primary" href="dash.jsp" role="button">Back</a>

              
                <section class="py-5">
                    <div class="container p-0">
                        <div class="row">
                            <!-- SHOP SIDEBAR-->
                           
                            <!-- SHOP LISTING-->

                            <div class="col-lg-9 order-1 order-lg-2 mb-5 mb-lg-0">
                             
                                <div class="row">


                                    <!-- PRODUCT-->
                                    <c:forEach items="${requestScope.products}" var="p">
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="product text-center">
                                                <div class="mb-3 position-relative">
                                                    <div class="badge text-white bg-"></div><a class="d-block" href="productview?id=${p.id}"><img class="img-fluid w-100" src="${p.image}" alt="..."></a>
                                                    <div class="product-overlay">
                                                        <ul class="mb-0 list-inline">
                                                            <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-outline-dark" href="deleteProducts?id=${p.id}"><i class="fa-regular fa-trash"></i></a></li>
                                                            <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="productview?id=${p.id}">View</a></li>  
                                                            <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="modifyProduct?id=${p.id}" ><i class="fa-solid fa-screwdriver-wrench"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <h6> <a class="reset-anchor" href="productview?id=${p.id}">${p.name}</a></h6>
                                                    <fmt:formatNumber pattern="#,##0VND" value="${p.price}"></fmt:formatNumber>
                                                </div>
                                            </div>
                                    </c:forEach>

                                    <!-- PAGINATION-->
                                    <nav aria-label="Page navigation example">
                                        <c:set value="${requestScope.page}" var="page"  />
                                        <ul class="pagination justify-content-center justify-content-lg-end">
                                            <li class="page-item mx-1"><a class="page-link" href="#!" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                                                <c:forEach begin="${1}" end="${requestScope.num}" var="i"  >
                                                <li   " class="page-item mx-1 active"><a style="transition:scale 1s ease-in  " class="page-link hoverDat" href="listCRUD?page=${i}">${i}</a></li>
                                                </c:forEach>
                                            <li class="page-item ms-1"><a class="page-link" href="#!" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                </section>
            </div>
            <footer class="bg-dark text-white">
                <div class="container py-4">
                    <div class="row py-5">
                        <div class="col-md-4 mb-3 mb-md-0">
                            <h6 class="text-uppercase mb-3">Customer services</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a class="footer-link" href="#!">Help &amp; Contact Us</a></li>
                                <li><a class="footer-link" href="#!">Returns &amp; Refunds</a></li>
                                <li><a class="footer-link" href="#!">Online Stores</a></li>
                                <li><a class="footer-link" href="#!">Terms &amp; Conditions</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 mb-3 mb-md-0">
                            <h6 class="text-uppercase mb-3">Company</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a class="footer-link" href="#!">What We Do</a></li>
                                <li><a class="footer-link" href="#!">Available Services</a></li>
                                <li><a class="footer-link" href="#!">Latest Posts</a></li>
                                <li><a class="footer-link" href="#!">FAQs</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h6 class="text-uppercase mb-3">Social media</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a class="footer-link" href="#!">Twitter</a></li>
                                <li><a class="footer-link" href="#!">Instagram</a></li>
                                <li><a class="footer-link" href="#!">Tumblr</a></li>
                                <li><a class="footer-link" href="#!">Pinterest</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="border-top pt-4" style="border-color: #1d1d1d !important">
                        <div class="row">
                            <div class="col-md-6 text-center text-md-start">
                                <p class="small text-muted mb-0">&copy; 2021 All rights reserved.</p>
                            </div>
                            <div class="col-md-6 text-center text-md-end">
                                <p class="small text-muted mb-0">Template designed by <a class="text-white reset-anchor" href="https://bootstrapious.com/p/boutique-bootstrap-e-commerce-template">Bootstrapious</a></p>
                                <!-- If you want to remove the backlink, please purchase the Attribution-Free License. See details in readme.txt or license.txt. Thanks!-->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- JavaScript files-->
            <script>
                var submit = function () {
                    documents.sort.submit();

                }

            </script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="vendor/glightbox/js/glightbox.min.js"></script>
            <script src="vendor/nouislider/nouislider.min.js"></script>
            <script src="vendor/swiper/swiper-bundle.min.js"></script>
            <script src="vendor/choices.js/public/assets/scripts/choices.min.js"></script>
            <script src="js/front.js"></script>
            <!-- Nouislider Config-->

            <script>
                // ------------------------------------------------------- //
                //   Inject SVG Sprite - 
                //   see more here 
                //   https://css-tricks.com/ajaxing-svg-sprite/
                // ------------------------------------------------------ //
                function injectSvgSprite(path) {

                    var ajax = new XMLHttpRequest();
                    ajax.open("GET", path, true);
                    ajax.send();
                    ajax.onload = function (e) {
                        var div = document.createElement("div");
                        div.className = 'd-none';
                        div.innerHTML = ajax.responseText;
                        document.body.insertBefore(div, document.body.childNodes[0]);
                    }
                }
                // this is set to BootstrapTemple website as you cannot 
                // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
                // while using file:// protocol
                // pls don't forget to change to your domain :)
                injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');

            </script>
            <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>


